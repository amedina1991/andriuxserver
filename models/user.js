
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var UserSchema = Schema({
    nombre:String ,
    apellido:String,
});

// Export the model
module.exports = mongoose.model('user', UserSchema);

