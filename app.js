
const User = require('./models/user');
let express = require('express');
let app = express();
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var cors = require('cors');

var bodyParser = require('body-parser');

// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(cors({credentials: true, origin: 'http://localhost:4200'}));

let http = require('http');
let server = http.Server(app);

let socketIO = require('socket.io');
let io = socketIO(server);

const port = process.env.PORT || 9000;

const conecction = mongoose.connect('mongodb://127.0.0.1:27017/usuarios')
    .then(db => console.log('db connected') )
    .catch(err => console.log(err));

let UserSchema = Schema({ nombre: String ,apellido: String},{ versionKey: false });

var usuarios =  mongoose.model('usuarios', UserSchema,'usuarios');

    io.on('connection', (socket) => {
        console.log('user connected');
        socket.on('new-message', (message) => {
            io.emit('new-message', message+"chamaco moreno");
        });
    });

    app.use('/all', async (req, res) => {
        const user = await usuarios.find();
        res.send(user);
    });

    app.use('/add', async (req,res) => {
        const user =  new usuarios(req.body);
        await user.save(function (err) {
            if (err) {
                res.json({msg:'Saving first user failed!'});
            } else {
                res.json({msg:'great'});
            }
        });
    });


    server.listen(port, () => {
        console.log(`started on port: ${port}`);
    });

